package com.tjcloud.core.cache;

import java.util.HashMap;
import java.util.Map;

public class MCache {
    private static Map<String, Object> caches = new HashMap<>(500);

    public static void add(String key, Object value) {
        caches.put(key, value);
    }

    public static Object get(String key) {
        return caches.get(key);
    }

    public static String getString(String key) {
        return (String)caches.get(key);
    }

    public static void clear() {
        caches.clear();
    }

    public static void remove(String key) {
        caches.remove(key);
    }

    public static boolean isEmpty() {
        return caches.isEmpty();
    }
}