package com.tjcloud.core.service;

import com.tjcloud.core.exception.ServiceException;
import com.tjcloud.core.page.PageParam;
import com.tjcloud.core.page.Pagination;
import java.util.List;
import java.util.Map;

public interface AbstractService<T extends com.tjcloud.core.entity.AbstractEntity, ID extends java.io.Serializable> {
  T save(T paramT) throws ServiceException;

  T create(T paramT) throws ServiceException;

  int modify(T paramT) throws ServiceException;

  int remove(ID paramID) throws ServiceException;

  int remove(T paramT) throws ServiceException;

  boolean exists(ID paramID) throws ServiceException;

  T get(ID paramID) throws ServiceException;

  T findOne(Map<String, Object> paramMap) throws ServiceException;

  List<T> findBy(Map<String, Object> paramMap) throws ServiceException;

  Pagination<T> queryPage(PageParam paramPageParam, Map<String, Object> paramMap) throws ServiceException;
}