package com.tjcloud.core.placeholder;

import com.tjcloud.core.utils.crypto.Cryption;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class MPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {
    protected static final String JEDIS_PASSWORD = "redis.password";

    protected static final String JDBC_PASSWORD = "jdbc.password";

    protected static final String SECRET_KEY_DEFAULT = "12345688";

    protected String convertProperty(String propertyName, String propertyValue) {
        if ("jdbc.password".equals(propertyName) || "redis.password".equals(propertyName))
            return Cryption.decryption(propertyValue, "12345688");
        return convertPropertyValue(propertyValue);
    }
}