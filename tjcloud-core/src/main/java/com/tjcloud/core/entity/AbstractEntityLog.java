package com.tjcloud.core.entity;

public abstract class AbstractEntityLog extends AbstractEntity {
    private static final long serialVersionUID = -7018528945717931887L;

    private Long oldId;

    private Long logTime;

    private Boolean isNew;

    public Long getOldId() {
        return this.oldId;
    }

    public void setOldId(Long oldId) {
        this.oldId = oldId;
    }

    public Long getLogTime() {
        return this.logTime;
    }

    public void setLogTime(Long logTime) {
        this.logTime = logTime;
    }

    public Boolean getIsNew() {
        return this.isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }
}