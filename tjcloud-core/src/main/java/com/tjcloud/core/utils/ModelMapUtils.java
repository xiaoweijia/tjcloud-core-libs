package com.tjcloud.core.utils;

import com.tjcloud.core.entity.ModelMap;

public class ModelMapUtils {
    public static ModelMap toModelMap(Object object) {
        return JsonUtils.<ModelMap>parseJavaObject(JsonUtils.toJsonString(object), ModelMap.class);
    }

    public static <T> T parseJavaObject(ModelMap model, Class<T> cls) {
        return JsonUtils.parseJavaObject(JsonUtils.toJsonString(model), cls);
    }
}