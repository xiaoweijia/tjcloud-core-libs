package com.tjcloud.core.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class DateUtils {
    public static final String DATE_FORMAT_FULL = "yyyy-MM-dd HH:mm:ss";

    public static int getDayOfMonth() {
        return (new DateTime()).getDayOfMonth();
    }

    public static String getDateTimeNow() {
        return (new DateTime()).toString("yyyyMMddHHmmss");
    }

    public static String getCurrentTime() {
        return (new DateTime()).toString("yyyy-MM-dd HH:mm:ss");
    }

    public static String getCurrentDate() {
        return (new DateTime()).toString("yyyy-MM-dd");
    }

    public static String getCurrentDate(String format) {
        return (new DateTime()).toString(format);
    }

    public static long getCurrentMills(String format) {
        return toLong((new DateTime()).toString(format), format);
    }

    public static Date getDate(String s) {
        return getDate(s, (String)null);
    }

    public static Date getJustDate(String s) {
        return getDate(s, "yyyy-MM-dd");
    }

    public static Date getDate(long date) {
        return getDate(date, (String)null);
    }

    public static Date getJustDate(long date) {
        return getDate(date, "yyyy-MM-dd");
    }

    public static Date getDate(long date, String format) {
        return getDate(formatDate(new Date(date), msDateFormat(format)), msDateFormat(format));
    }

    public static Date getDate(String s, String format) {
        Date date;
        try {
            date = (new SimpleDateFormat(msDateFormat(format))).parse(s);
        } catch (Exception e) {
            date = new Date(0L);
        }
        return date;
    }

    public static String formatDate(Long time, String format) {
        if (time == null)
            return "";
        return formatDate(new Date(time.longValue()), format);
    }

    public static String formatDate(Long time) {
        if (time == null)
            return "";
        return formatDate(new Date(time.longValue()), (String)null);
    }

    public static String formatJustDate(Long time) {
        if (time == null)
            return "";
        return formatDate(new Date(time.longValue()), "yyyy-MM-dd");
    }

    public static String formatDate(Date date, String format) {
        return (new SimpleDateFormat(msDateFormat(format))).format(date);
    }

    private static String msDateFormat(String format) {
        if (StringUtils.isEmpty(format))
            format = "yyyy-MM-dd HH:mm:ss";
        return format;
    }

    public static long toLong(String date) {
        return toLong(date, "yyyy-MM-dd");
    }

    public static long toLong(String date, String format) {
        return DateTime.parse(date, DateTimeFormat.forPattern(format)).getMillis();
    }

    public static long toLongDateTime(String date) {
        if (date.length() == 16)
            return toLong(date, "yyyy-MM-dd HH:mm");
        return toLong(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static String toString(Long time) {
        if (time == null)
            return "";
        return (new DateTime(time)).toString("yyyy-MM-dd");
    }

    public static String toString(Long time, String format) {
        if (time == null)
            return "";
        return (new DateTime(time)).toString(format);
    }

    public static String toFullString(Long time) {
        if (time == null)
            return "";
        return (new DateTime(time)).toString("yyyy-MM-dd HH:mm:ss");
    }

    public static long minusHours(long date, int hours) {
        return (new DateTime(date)).minusHours(hours).getMillis();
    }

    public static long minusHours(String date, String format, int hours) {
        return DateTime.parse(date, DateTimeFormat.forPattern(format)).minusHours(hours).getMillis();
    }

    public static String minusHours(long date, String format, int hours) {
        return (new DateTime(date)).minusHours(hours).toString(format);
    }

    public static long minusDays(long date, int days) {
        return (new DateTime(date)).minusDays(days).getMillis();
    }

    public static long minusDays(String date, String format, int days) {
        return DateTime.parse(date, DateTimeFormat.forPattern(format)).minusDays(days).getMillis();
    }

    public static String minusDays(long date, String format, int days) {
        return (new DateTime(date)).minusDays(days).toString(format);
    }

    public static long minusMonths(long date, int months) {
        return (new DateTime(date)).minusMonths(months).getMillis();
    }

    public static long minusMonths(String date, String format, int months) {
        return DateTime.parse(date, DateTimeFormat.forPattern(format)).minusMonths(months).getMillis();
    }

    public static String minusMonths(long date, String format, int months) {
        return (new DateTime(date)).minusMonths(months).toString(format);
    }

    public static long minusYears(long date, int years) {
        return (new DateTime(date)).minusYears(years).getMillis();
    }

    public static long minusYears(String date, String format, int years) {
        return DateTime.parse(date, DateTimeFormat.forPattern(format)).minusYears(years).getMillis();
    }

    public static String minusYears(long date, String format, int years) {
        return (new DateTime(date)).minusYears(years).toString(format);
    }

    public static long plusHours(long date, int hours) {
        return (new DateTime(date)).plusHours(hours).getMillis();
    }

    public static long plusHours(String date, String format, int hours) {
        return DateTime.parse(date, DateTimeFormat.forPattern(format)).plusHours(hours).getMillis();
    }

    public static String plusHours(long date, String format, int hours) {
        return (new DateTime(date)).plusHours(hours).toString(format);
    }

    public static long plusDays(long date, int days) {
        return (new DateTime(date)).plusDays(days).getMillis();
    }

    public static long plusDays(String date, String format, int days) {
        return DateTime.parse(date, DateTimeFormat.forPattern(format)).plusDays(days).getMillis();
    }

    public static String plusDays(long date, String format, int days) {
        return (new DateTime(date)).plusDays(days).toString(format);
    }

    public static long plusMonths(long date, int months) {
        return (new DateTime(date)).plusMonths(months).getMillis();
    }

    public static long plusMonths(String date, String format, int months) {
        return DateTime.parse(date, DateTimeFormat.forPattern(format)).plusMonths(months).getMillis();
    }

    public static String plusMonths(long date, String format, int months) {
        return (new DateTime(date)).plusMonths(months).toString(format);
    }

    public static long plusYears(long date, int years) {
        return (new DateTime(date)).plusYears(years).getMillis();
    }

    public static long plusYears(String date, String format, int years) {
        return DateTime.parse(date, DateTimeFormat.forPattern(format)).plusYears(years).getMillis();
    }

    public static String plusYears(long date, String format, int years) {
        return (new DateTime(date)).plusYears(years).toString(format);
    }

    public static String getDayzone() {
        String[] dayTime = { "早上", "上午", "中午", "下午", "晚上"};
        int hour = DateTime.now().getHourOfDay();
        if (hour < 5)
            return dayTime[4];
        if (hour < 8)
            return dayTime[0];
        if (hour < 12)
            return dayTime[1];
        if (hour < 14)
            return dayTime[2];
        if (hour < 18)
            return dayTime[3];
        return dayTime[4];
  }

   public static void main(String[] args) {
     Calendar begin = new GregorianCalendar(2016, 0, 1);
     Calendar end = new GregorianCalendar(2016, 0, 3, 23, 59, 59);
     print("元旦", begin, end);
     begin = new GregorianCalendar(2016, 1, 7);
     end = new GregorianCalendar(2016, 1, 13, 23, 59, 59);
     print("春节", begin, end);
     begin = new GregorianCalendar(2016, 3, 4);
     end = new GregorianCalendar(2016, 3, 6, 23, 59, 59);
     print("清明节", begin, end);
     begin = new GregorianCalendar(2016, 4, 1);
     end = new GregorianCalendar(2016, 4, 3, 23, 59, 59);
     print("劳动节", begin, end);
     begin = new GregorianCalendar(2016, 5, 9);
     end = new GregorianCalendar(2016, 5, 11, 23, 59, 59);
     print("端午节", begin, end);
     begin = new GregorianCalendar(2016, 8, 15);
     end = new GregorianCalendar(2016, 8, 17, 23, 59, 59);
     print("中秋节", begin, end);
     begin = new GregorianCalendar(2016, 9, 1);
     end = new GregorianCalendar(2016, 9, 7, 23, 59, 59);
     print("国庆节", begin, end);
     begin = new GregorianCalendar(2016, 0, 29, 7, 30);
     end = new GregorianCalendar(2016, 0, 29, 17, 59, 59);
     print("工作日", begin, end);
     Calendar day = Calendar.getInstance();
     day.setTimeInMillis(System.currentTimeMillis());
     int dayOfWeek = day.get(7);
     if (dayOfWeek == 7 || dayOfWeek == 1)
        {
           System.out.println("today is weekend.");
         }
     System.out.println("day of week :" + dayOfWeek);
     System.out.println(System.currentTimeMillis());
     System.out.println((new Date()).getTime());
   }

   public static void print(String holiday, Calendar begin, Calendar end) {
         SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         System.out.println(holiday + " ::: " + begin.getTimeInMillis() + " - " + end.getTimeInMillis() + "  ::::  " + df.format(begin.getTime()) + " ~ " + df.format(end.getTime()));
   }
}
