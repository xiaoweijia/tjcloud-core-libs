package com.tjcloud.core.utils.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Cryption {
    private static final Logger logger = LoggerFactory.getLogger(Cryption.class);

    private static final String DES_ALGORITHM = "DES/CBC/PKCS5Padding";

    public static String encryption(String plainData, String secretKey) {
        String result = null;
        try {
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(secretKey.getBytes("UTF-8"));
            cipher.init(1, generateKey(secretKey), ivParameterSpec);
            byte[] buf = cipher.doFinal(plainData.getBytes());
            result = Base64.encodeBase64String(buf);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    public static String decryption(String secretData, String secretKey) {
        String result = null;
        try {
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(secretKey.getBytes("UTF-8"));
            cipher.init(2, generateKey(secretKey), ivParameterSpec);
            byte[] buf = cipher.doFinal(Base64.decodeBase64(secretData));
            result = new String(buf, "utf-8");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    private static SecretKey generateKey(String secretKey) {
        try {
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            DESKeySpec keySpec = new DESKeySpec(secretKey.getBytes());
            return keyFactory.generateSecret(keySpec);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException("generate secretKey fail !!");
        }
    }

    private static String byteArray2HexStingr(byte[] bs) {
        StringBuffer sb = new StringBuffer();
        byte[] arrayOfByte = bs;
        int j = bs.length;
        for (int i = 0; i < j; i++) {
            byte b = arrayOfByte[i];
            sb.append(byte2HexString(b));
        }
        return sb.toString();
    }

    private static String byte2HexString(byte b) {
        String hexStr = null;
        int n = b;
        if (n < 0)
            n = b & 0xFF;
        hexStr = Integer.toHexString(n / 16) + Integer.toHexString(n % 16);
        return hexStr.toUpperCase();
    }

    public static String md5Hex(String origString) {
        String origMD5 = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] result = md5.digest(origString.getBytes("UTF-8"));
            origMD5 = byteArray2HexStingr(result);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return origMD5;
    }

    public static String encodePassword(String rawPass, Object salt) {
        String saltedPass = mergePasswordAndSalt(rawPass, salt, false);
        MessageDigest messageDigest = getMessageDigest();
        byte[] digest = messageDigest.digest(Utf8.encode(saltedPass));
        return new String(Hex.encode(digest));
    }

    protected static final MessageDigest getMessageDigest() throws IllegalArgumentException {
        try {
            return MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException("No such algorithm [1]");
        }
    }

    protected static String mergePasswordAndSalt(String password, Object salt, boolean strict) {
        if (password == null)
            password = "";
        if (strict && salt != null && (
                salt.toString().lastIndexOf("{") != -1 || salt.toString().lastIndexOf("}") != -1))
            throw new IllegalArgumentException("Cannot use { or } in salt.toString()");
        if (salt == null || "".equals(salt))
            return password;
        return password + "{" + salt.toString() + "}";
    }
}