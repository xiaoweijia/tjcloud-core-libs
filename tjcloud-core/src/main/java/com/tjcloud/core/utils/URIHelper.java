package com.tjcloud.core.utils;

import java.net.URI;

public class URIHelper {
    public static String getPassword(URI uri) {
        String userInfo = uri.getUserInfo();
        if (userInfo != null)
            return userInfo.split(":", 2)[1];
        return null;
    }

    public static boolean isValid(URI uri) {
        if (StringUtils.isEmpty(uri.getScheme()) || StringUtils.isEmpty(uri.getHost()) || uri.getPort() == -1)
            return false;
        return true;
    }
}