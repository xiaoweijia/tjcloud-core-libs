package com.tjcloud.core.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class WebUtils {
    private static final Logger logger = LoggerFactory.getLogger(WebUtils.class);

    private static final Log clog = LogFactory.getLog("sdk.comm.err");

    public static final String DATE_TIMEZONE = "GMT+8";

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String SDK_VERSION = "tjcloud-sdk-java";

    public static final String CHARSET_UTF8 = "UTF-8";

    public static final String DEFAULT_CHARSET = "UTF-8";

    public static final String METHOD_POST = "POST";

    public static final String METHOD_GET = "GET";

    public static final String APPLICATION_JSON_VALUE = "application/json";

    public static final String APPLICATION_JSON_UTF8_VALUE = "application/json;charset=UTF-8";

    private static SSLContext ctx = null;

    private static HostnameVerifier verifier = null;

    private static SSLSocketFactory socketFactory = null;

    private static String ip = null;

    private static String osName = System.getProperties().getProperty("os.name");

    public static final int connectTimeout = 3000;

    public static final int readTimeout = 15000;

    private static class DefaultTrustManager implements X509TrustManager {
        private DefaultTrustManager() {}

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {}

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
    }

    static {
        try {
            ctx = SSLContext.getInstance("TLS");
            ctx.init(new javax.net.ssl.KeyManager[0], new TrustManager[] { new DefaultTrustManager() }, new SecureRandom());
            ctx.getClientSessionContext().setSessionTimeout(15);
            ctx.getClientSessionContext().setSessionCacheSize(1000);
            socketFactory = ctx.getSocketFactory();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        verifier = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return false;
            }
        };
    }

    public static String doPost(String url, Map<String, String> params, int connectTimeout, int readTimeout) throws IOException {
        return doPost(url, params, "UTF-8", connectTimeout, readTimeout);
    }

    public static String doPost(String url, Map<String, String> params, String charset, int connectTimeout, int readTimeout) throws IOException {
        String ctype = "application/x-www-form-urlencoded;charset=" + charset;
        String query = buildQuery(params, charset);
        byte[] content = new byte[0];
        if (query != null)
            content = query.getBytes(charset);
        return doPost(url, ctype, content, connectTimeout, readTimeout);
    }

    public static String doPost(String url, String ctype, byte[] content, int connectTimeout, int readTimeout) throws IOException {
        HttpURLConnection conn = null;
        OutputStream out = null;
        String rsp = null;
        try {
            try {
                conn = getConnection(new URL(url), "POST", ctype);
                conn.setConnectTimeout(connectTimeout);
                conn.setReadTimeout(readTimeout);
            } catch (IOException e) {
                Map<String, String> map = getParamsFromUrl(url);
                logCommError(e, url, map.get("app_key"), map.get("method"), content);
                throw e;
            }
            try {
                out = conn.getOutputStream();
                out.write(content);
                rsp = getResponseAsString(conn);
            } catch (IOException e) {
                Map<String, String> map = getParamsFromUrl(url);
                logCommError(e, conn, map.get("app_key"), map.get("method"), content);
                throw e;
            }
        } finally {
            if (out != null)
                out.close();
            if (conn != null)
                conn.disconnect();
        }
        return rsp;
    }

    public static String doPost(String url, Map<String, String> params, Map<String, FileItem> fileParams, int connectTimeout, int readTimeout) throws IOException {
        if (fileParams == null || fileParams.isEmpty())
            return doPost(url, params, "UTF-8", connectTimeout, readTimeout);
        return doPost(url, params, fileParams, "UTF-8", connectTimeout, readTimeout);
    }

    public static String doPost(String url, Map<String, String> params, Map<String, FileItem> fileParams, String charset, int connectTimeout, int readTimeout) throws IOException {
        if (fileParams == null || fileParams.isEmpty())
            return doPost(url, params, charset, connectTimeout, readTimeout);
        String boundary = System.currentTimeMillis() + "";
        HttpURLConnection conn = null;
        OutputStream out = null;
        String rsp = null;
        try {
            try {
                String ctype = "multipart/form-data;boundary=" + boundary + ";charset=" + charset;
                conn = getConnection(new URL(url), "POST", ctype);
                conn.setConnectTimeout(connectTimeout);
                conn.setReadTimeout(readTimeout);
            } catch (IOException e) {
                Map<String, String> map = getParamsFromUrl(url);
                logCommError(e, url, map.get("app_key"), map.get("method"), params);
                throw e;
            }
            try {
                out = conn.getOutputStream();
                byte[] entryBoundaryBytes = ("\r\n--" + boundary + "\r\n").getBytes(charset);
                Set<Map.Entry<String, String>> textEntrySet = params.entrySet();
                for (Map.Entry<String, String> textEntry : textEntrySet) {
                    byte[] textBytes = getTextEntry(textEntry.getKey(), textEntry.getValue(), charset);
                    out.write(entryBoundaryBytes);
                    out.write(textBytes);
                }
                Set<Map.Entry<String, FileItem>> fileEntrySet = fileParams.entrySet();
                for (Map.Entry<String, FileItem> fileEntry : fileEntrySet) {
                    FileItem fileItem = fileEntry.getValue();
                    byte[] fileBytes = getFileEntry(fileEntry.getKey(), fileItem.getFileName(), fileItem.getMimeType(), charset);
                    out.write(entryBoundaryBytes);
                    out.write(fileBytes);
                    out.write(fileItem.getContent());
                }
                byte[] endBoundaryBytes = ("\r\n--" + boundary + "--\r\n").getBytes(charset);
                out.write(endBoundaryBytes);
                rsp = getResponseAsString(conn);
            } catch (IOException e) {
                Map<String, String> map = getParamsFromUrl(url);
                logCommError(e, conn, map.get("app_key"), map.get("method"), params);
                throw e;
            }
        } finally {
            if (out != null)
                out.close();
            if (conn != null)
                conn.disconnect();
        }
        return rsp;
    }

    private static byte[] getTextEntry(String fieldName, String fieldValue, String charset) throws IOException {
        StringBuilder entry = new StringBuilder();
        entry.append("Content-Disposition:form-data;name=\"");
        entry.append(fieldName);
        entry.append("\"\r\nContent-Type:text/plain\r\n\r\n");
        entry.append(fieldValue);
        return entry.toString().getBytes(charset);
    }

    private static byte[] getFileEntry(String fieldName, String fileName, String mimeType, String charset) throws IOException {
        StringBuilder entry = new StringBuilder();
        entry.append("Content-Disposition:form-data;name=\"");
        entry.append(fieldName);
        entry.append("\";filename=\"");
        entry.append(fileName);
        entry.append("\"\r\nContent-Type:");
        entry.append(mimeType);
        entry.append("\r\n\r\n");
        return entry.toString().getBytes(charset);
    }

    public static String doGet(String url, Map<String, String> params) throws IOException {
        return doGet(url, params, "UTF-8");
    }

    public static String doGet(String url, Map<String, String> params, String charset) throws IOException {
        HttpURLConnection conn = null;
        String rsp = null;
        try {
            String ctype = "application/x-www-form-urlencoded;charset=" + charset;
            String query = buildQuery(params, charset);
            try {
                conn = getConnection(buildGetUrl(url, query), "GET", ctype);
            } catch (IOException e) {
                Map<String, String> map = getParamsFromUrl(url);
                logCommError(e, url, map.get("app_key"), map.get("method"), params);
                throw e;
            }
            try {
                rsp = getResponseAsString(conn);
            } catch (IOException e) {
                Map<String, String> map = getParamsFromUrl(url);
                logCommError(e, conn, map.get("app_key"), map.get("method"), params);
                throw e;
            }
        } finally {
            if (conn != null)
                conn.disconnect();
        }
        return rsp;
    }

    private static HttpURLConnection getConnection(URL url, String method, String ctype) throws IOException {
        HttpURLConnection conn = null;
        if ("https".equals(url.getProtocol())) {
            HttpsURLConnection connHttps = (HttpsURLConnection)url.openConnection();
            connHttps.setSSLSocketFactory(socketFactory);
            connHttps.setHostnameVerifier(verifier);
            conn = connHttps;
        } else {
            conn = (HttpURLConnection)url.openConnection();
        }
        conn.setRequestMethod(method);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("Accept", "text/xml,text/javascript,text/html");
        conn.setRequestProperty("User-Agent", "aop-sdk-java");
        conn.setRequestProperty("Content-Type", ctype);
        return conn;
    }

    private static URL buildGetUrl(String strUrl, String query) throws IOException {
        URL url = new URL(strUrl);
        if (StringUtils.isEmpty(query))
            return url;
        if (StringUtils.isEmpty(url.getQuery())) {
            if (strUrl.endsWith("?")) {
                strUrl = strUrl + query;
            } else {
                strUrl = strUrl + "?" + query;
            }
        } else if (strUrl.endsWith("&")) {
            strUrl = strUrl + query;
        } else {
            strUrl = strUrl + "&" + query;
        }
        return new URL(strUrl);
    }

    public static String buildQuery(Map<String, String> params, String charset) throws IOException {
        if (params == null || params.isEmpty())
            return null;
        StringBuilder query = new StringBuilder();
        Set<Map.Entry<String, String>> entries = params.entrySet();
        boolean hasParam = false;
        for (Map.Entry<String, String> entry : entries) {
            String name = entry.getKey();
            String value = entry.getValue();
            if (StringUtils.areNotEmpty(new String[] { name, value })) {
                if (hasParam) {
                    query.append("&");
                } else {
                    hasParam = true;
                }
                query.append(name).append("=").append(URLEncoder.encode(value, charset));
            }
        }
        return query.toString();
    }

    protected static String getResponseAsString(HttpURLConnection conn) throws IOException {
        String charset = getResponseCharset(conn.getContentType());
        InputStream es = conn.getErrorStream();
        if (es == null)
            return getStreamAsString(conn.getInputStream(), charset);
        String msg = getStreamAsString(es, charset);
        if (StringUtils.isEmpty(msg))
            throw new IOException(conn.getResponseCode() + ":" + conn.getResponseMessage());
        throw new IOException(msg);
    }

    private static String getStreamAsString(InputStream stream, String charset) throws IOException {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, charset));
            StringWriter writer = new StringWriter();
            char[] chars = new char[256];
            int count = 0;
            while ((count = reader.read(chars)) > 0)
                writer.write(chars, 0, count);
            return writer.toString();
        } finally {
            if (stream != null)
                stream.close();
        }
    }

    private static String getResponseCharset(String ctype) {
        String charset = "UTF-8";
        if (!StringUtils.isEmpty(ctype)) {
            String[] params = ctype.split(";");
            for (String param : params) {
                param = param.trim();
                if (param.startsWith("charset")) {
                    String[] pair = param.split("=", 2);
                    if (pair.length == 2 &&
                            !StringUtils.isEmpty(pair[1]))
                        charset = pair[1].trim();
                    break;
                }
            }
        }
        return charset;
    }

    public static String decode(String value) {
        return decode(value, "UTF-8");
    }

    public static String encode(String value) {
        return encode(value, "UTF-8");
    }

    public static String decode(String value, String charset) {
        String result = null;
        if (!StringUtils.isEmpty(value))
            try {
                result = URLDecoder.decode(value, charset);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        return result;
    }

    public static String encode(String value, String charset) {
        String result = null;
        if (!StringUtils.isEmpty(value))
            try {
                result = URLEncoder.encode(value, charset);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        return result;
    }

    private static Map<String, String> getParamsFromUrl(String url) {
        Map<String, String> map = null;
        if (url != null && url.indexOf('?') != -1)
            map = splitUrlQuery(url.substring(url.indexOf('?') + 1));
        if (map == null)
            map = new HashMap<>();
        return map;
    }

    public static Map<String, String> splitUrlQuery(String query) {
        Map<String, String> result = new HashMap<>();
        String[] pairs = query.split("&");
        if (pairs != null && pairs.length > 0)
            for (String pair : pairs) {
                String[] param = pair.split("=", 2);
                if (param != null && param.length == 2)
                    result.put(param[0], param[1]);
            }
        return result;
    }

    public static String buildForm(String baseUrl, Map<String, String> parameters) {
        StringBuffer sb = new StringBuffer();
        sb.append("<form name=\"punchout_form\" method=\"post\" action=\"");
        sb.append(baseUrl);
        sb.append("\">\n");
        sb.append(buildHiddenFields(parameters));
        sb.append("<input type=\"submit\" value=\"\" style=\"display:none\" >\n");
        sb.append("</form>\n");
        sb.append("<script>document.forms[0].submit();</script>");
        String form = sb.toString();
        return form;
    }

    private static String buildHiddenFields(Map<String, String> parameters) {
        if (parameters == null || parameters.isEmpty())
            return "";
        StringBuffer sb = new StringBuffer();
        Set<String> keys = parameters.keySet();
        for (String key : keys) {
            String value = parameters.get(key);
            if (key == null || value == null)
                continue;
            sb.append(buildHiddenField(key, value));
        }
        String result = sb.toString();
        return result;
    }

    private static String buildHiddenField(String key, String value) {
        StringBuffer sb = new StringBuffer();
        sb.append("<input type=\"hidden\" name=\"");
        sb.append(key);
        sb.append("\" value=\"");
        String a = value.replace("\"", "&quot;");
        sb.append(a).append("\">\n");
        return sb.toString();
    }

    public static void logCommError(Exception e, HttpURLConnection conn, String appKey, String method, byte[] content) {
        String contentString = null;
        try {
            contentString = new String(content, "UTF-8");
            logCommError(e, conn, appKey, method, contentString);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static void logCommError(Exception e, String url, String appKey, String method, byte[] content) {
        String contentString = null;
        try {
            contentString = new String(content, "UTF-8");
            logCommError(e, url, appKey, method, contentString);
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
    }

    public static void logCommError(Exception e, HttpURLConnection conn, String appKey, String method, Map<String, String> params) {
        _logCommError(e, conn, null, appKey, method, params);
    }

    public static void logCommError(Exception e, String url, String appKey, String method, Map<String, String> params) {
        _logCommError(e, null, url, appKey, method, params);
    }

    private static void logCommError(Exception e, HttpURLConnection conn, String appKey, String method, String content) {
        Map<String, String> params = parseParam(content);
        _logCommError(e, conn, null, appKey, method, params);
    }

    private static void logCommError(Exception e, String url, String appKey, String method, String content) {
        Map<String, String> params = parseParam(content);
        _logCommError(e, null, url, appKey, method, params);
    }

    private static void _logCommError(Exception e, HttpURLConnection conn, String url, String appKey, String method, Map<String, String> params) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        String sdkName = "tjcloud-sdk-java";
        String urlStr = null;
        String rspCode = "";
        if (conn != null) {
            try {
                urlStr = conn.getURL().toString();
                rspCode = "HTTP_ERROR_" + conn.getResponseCode();
            } catch (IOException iOException) {}
        } else {
            urlStr = url;
            rspCode = "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(df.format(new Date()));
        sb.append("^_^");
        sb.append(method);
        sb.append("^_^");
        sb.append(appKey);
        sb.append("^_^");
        sb.append(getIp());
        sb.append("^_^");
        sb.append(osName);
        sb.append("^_^");
        sb.append(sdkName);
        sb.append("^_^");
        sb.append(urlStr);
        sb.append("^_^");
        sb.append(rspCode);
        sb.append("^_^");
        sb.append((e.getMessage() + "").replaceAll("\r\n", " "));
        clog.error(sb.toString());
    }

    private static Map<String, String> parseParam(String contentString) {
        Map<String, String> params = new HashMap<>();
        if (contentString == null || contentString.trim().equals(""))
            return params;
        String[] paramsArray = contentString.split("\\&");
        if (paramsArray != null)
            for (String param : paramsArray) {
                String[] keyValue = param.split("=");
                if (keyValue != null && keyValue.length == 2)
                    params.put(keyValue[0], keyValue[1]);
            }
        return params;
    }

    public static String getIp() {
        if (ip == null)
            try {
                ip = InetAddress.getLocalHost().getHostAddress();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        return ip;
    }

    public static String getRemoteHost(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (unknownIp(ip))
            ip = request.getHeader("Proxy-Client-IP");
        if (unknownIp(ip))
            ip = request.getHeader("WL-Proxy-Client-IP");
        if (unknownIp(ip))
            ip = request.getHeader("HTTP_CLIENT_IP");
        if (unknownIp(ip))
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        if (unknownIp(ip))
            ip = request.getRemoteAddr();
        return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
    }

    private static boolean unknownIp(String ip) {
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
            return true;
        return false;
    }

    public static String doGet(String url) throws IOException {
        return doGet(url, "");
    }

    public static String doGet(String url, String authorization) throws IOException {
        logger.debug("^_^ do get start. url {}, authorization {}", url, authorization);
        String jsonStr = "";
        try {
            CloseableHttpClient httpclient = HttpClientBuilder.create().build();
            HttpGet httpget = new HttpGet(url);
            httpget.setHeader("Content-Type", "application/json;charset=UTF-8");
            if (StringUtils.isNotEmpty(authorization))
                httpget.setHeader("Authorization", authorization);
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(3000).setSocketTimeout(15000).build();
            httpget.setConfig(requestConfig);
            CloseableHttpResponse response = httpclient.execute((HttpUriRequest)httpget);
            logger.debug("^_^ StatusCode -> " + response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            jsonStr = EntityUtils.toString(entity, "utf-8");
            logger.debug("^_^ do get response json result {}", jsonStr);
            httpget.releaseConnection();
        } catch (IOException e) {
            logger.error("^-^ Warning doGet Failed. url {}, authorization {}", url, authorization);
            logger.error(e.getMessage(), e);
        }
        return jsonStr;
    }

    public static String doPut(String url, String bizContent, String authorization) throws IOException {
        logger.debug("^_^ do put start. url {}, bizContent {}, authorization {}", new Object[] { url, bizContent, authorization });
        String jsonStr = "";
        try {
            CloseableHttpClient httpclient = HttpClientBuilder.create().build();
            HttpPut httpput = new HttpPut(url);
            httpput.setHeader("Content-Type", "application/json;charset=UTF-8");
            if (StringUtils.isNotEmpty(authorization))
                httpput.setHeader("Authorization", authorization);
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(3000).setSocketTimeout(15000).build();
            httpput.setConfig(requestConfig);
            if (StringUtils.isNotEmpty(bizContent))
                httpput.setEntity((HttpEntity)new StringEntity(bizContent));
            CloseableHttpResponse response = httpclient.execute((HttpUriRequest)httpput);
            logger.debug("^_^ StatusCode -> " + response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            jsonStr = EntityUtils.toString(entity, "utf-8");
            logger.debug("^_^ do put response json result {}", jsonStr);
            httpput.releaseConnection();
        } catch (IOException e) {
            logger.error("^-^ Warning doPut Failed. url {}, authorization {}, biz_content {}", new Object[] { url, authorization, bizContent });
            logger.error(e.getMessage(), e);
        }
        return jsonStr;
    }

    public static String doPost(String url, String bizContent, String authorization) throws IOException {
        logger.debug("^_^ do post start. url {}, bizContent {}, authorization {}", new Object[] { url, bizContent, authorization });
        String jsonStr = "";
        try {
            CloseableHttpClient httpclient = HttpClientBuilder.create().build();
            HttpPost httppost = new HttpPost(url);
            httppost.setHeader("Content-Type", "application/json;charset=UTF-8");
            if (StringUtils.isNotEmpty(authorization))
                httppost.setHeader("Authorization", authorization);
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(3000).setSocketTimeout(15000).build();
            httppost.setConfig(requestConfig);
            httppost.setEntity((HttpEntity)new StringEntity(bizContent));
            CloseableHttpResponse response = httpclient.execute((HttpUriRequest)httppost);
            logger.debug("^_^ StatusCode -> " + response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            jsonStr = EntityUtils.toString(entity, "utf-8");
            logger.debug("^_^ do post response json result {}", jsonStr);
            httppost.releaseConnection();
        } catch (IOException e) {
            logger.error("^-^ Warning doPost Failed. url {}, authorization {}, biz_content {}", new Object[] { url, authorization, bizContent });
            logger.error(e.getMessage(), e);
        }
        return jsonStr;
    }
}