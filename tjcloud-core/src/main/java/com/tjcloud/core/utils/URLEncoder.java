package com.tjcloud.core.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class URLEncoder {
    private static final Logger logger = LoggerFactory.getLogger(URLEncoder.class);
    private static final String CODEC_TABLE = "abcdefghijkmnoprstuvwxyz12345678";
    private static final int FIVE_BIT = 5;
    private static final int EIGHT_BIT = 8;
    private static final int BINARY = 2;

    public URLEncoder() {
    }

    public static String encode(String s) {
        return encode(s, (String)null);
    }

    public static String encode(String s, String charsetName) {
        if (StringUtils.isEmpty(s)) {
            return s;
        } else {
            byte[] keyBytes = null;
            if (StringUtils.isTrimEmpty(charsetName)) {
                keyBytes = s.getBytes();
            } else {
                try {
                    keyBytes = s.getBytes(charsetName);
                } catch (UnsupportedEncodingException var4) {
                }
            }

            return encode(keyBytes);
        }
    }

    public static String decode(String s) {
        return decode(s, (String)null);
    }

    public static String decode(String s, String charsetName) {
        if (StringUtils.isEmpty(s)) {
            return s;
        } else {
            StringBuilder sbBinarys = new StringBuilder();

            for(int i = 0; i < s.length(); ++i) {
                formatBinary(Integer.toBinaryString(getCodecTableIndex(s.charAt(i))), sbBinarys, 5);
            }

            byte[] binarys = new byte[sbBinarys.length() / 8];
            int i = 0;

            for(int j = 0; i < binarys.length; ++i) {
                int var10003 = j;
                j += 8;
                binarys[i] = Integer.valueOf(sbBinarys.substring(var10003, j), 2).byteValue();
            }

            String decoded = null;
            if (StringUtils.isTrimEmpty(charsetName)) {
                decoded = new String(binarys);
            } else {
                try {
                    return new String(binarys, charsetName);
                } catch (UnsupportedEncodingException var6) {
                }
            }

            return decoded;
        }
    }

    private static String encode(byte[] bs) {
        if (bs != null && bs.length >= 1) {
            StringBuilder mergrd = new StringBuilder();

            int groupCount;
            for(groupCount = 0; groupCount < bs.length; ++groupCount) {
                formatBinary(bs[groupCount], mergrd);
            }

            groupCount = mergrd.length() / 5;
            int lastCount = mergrd.length() % 5;
            if (lastCount > 0) {
                ++groupCount;
            }

            StringBuilder sbBinarys = new StringBuilder();
            int forMax = groupCount * 5;

            for(int i = 0; i < forMax; i += 5) {
                int end = i + 5;
                boolean flag = false;
                if (end > mergrd.length()) {
                    end = i + lastCount;
                    flag = true;
                }

                String strFiveBit = mergrd.substring(i, end);
                int intFiveBit = Integer.parseInt(strFiveBit, 2);
                if (flag) {
                    intFiveBit <<= 5 - lastCount;
                }

                sbBinarys.append("abcdefghijkmnoprstuvwxyz12345678".charAt(intFiveBit));
            }

            return sbBinarys.toString();
        } else {
            return "";
        }
    }

    private static int getCodecTableIndex(char c) {
        for(int i = 0; i < "abcdefghijkmnoprstuvwxyz12345678".length(); ++i) {
            if ("abcdefghijkmnoprstuvwxyz12345678".charAt(i) == c) {
                return i;
            }
        }

        return -1;
    }

    private static StringBuilder formatBinary(byte b, StringBuilder toAppendTo) {
        return formatBinary(b, toAppendTo, 8);
    }

    private static StringBuilder formatBinary(byte b, StringBuilder toAppendTo, int bitCount) {
        return formatBinary(Integer.toBinaryString(b), toAppendTo, bitCount);
    }

    private static StringBuilder formatBinary(String s, StringBuilder toAppendTo, int bitCount) {
        if (s != null && s.length() >= 1) {
            if (toAppendTo == null) {
                toAppendTo = new StringBuilder();
            }

            if (s.length() == bitCount) {
                return toAppendTo.append(s);
            } else if (s.length() >= bitCount) {
                return s.length() > bitCount ? toAppendTo.append(s.substring(s.length() - bitCount)) : toAppendTo;
            } else {
                StringBuilder formatted = new StringBuilder();
                formatted.append(s);

                do {
                    formatted.insert(0, "0");
                } while(formatted.length() < bitCount);

                return toAppendTo.append(formatted);
            }
        } else {
            return toAppendTo;
        }
    }

    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            while(true) {
                System.out.print("输入字符号串：");
                String in = br.readLine();
                if ("exit".equalsIgnoreCase(in)) {
                    break;
                }

                String enCode = encode(in, "GBK");
                String deCode = decode(enCode, "GBK");
                System.out.println();
                System.out.println("------------------------------");
                System.out.println("编码：" + enCode);
                System.out.println("解码：" + deCode);
                System.out.println("------------------------------");
                System.out.println();
            }
        } catch (IOException var5) {
            logger.error(var5.getMessage(), var5);
        }

    }
}