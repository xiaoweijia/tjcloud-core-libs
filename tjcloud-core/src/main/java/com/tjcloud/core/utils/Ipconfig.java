package com.tjcloud.core.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Ipconfig {
    private static final Logger logger = LoggerFactory.getLogger(Ipconfig.class);

    public static InetAddress getLocalHost() throws SocketException {
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getLocalHost();
            logger.debug("^_^ the local host is {}", inetAddress);
        } catch (UnknownHostException e) {
            logger.error(e.getMessage(), e);
        }
        return inetAddress;
    }

    public static String getLocalMacAddress() {
        String macAddress = "";
        try {
            macAddress = getMacAddress(InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            logger.error(e.getMessage(), e);
        } catch (SocketException e) {
            logger.error(e.getMessage(), e);
        }
        return macAddress;
    }

    public static String getMacAddress(InetAddress inetAddress) throws SocketException {
        byte[] mac = NetworkInterface.getByInetAddress(inetAddress).getHardwareAddress();
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < mac.length; i++) {
            if (i != 0)
                buf.append("-");
            String str = Integer.toHexString(mac[i] & 0xFF);
            buf.append((str.length() == 1) ? (Character.MIN_VALUE + str) : str);
        }
        String macAddress = buf.toString().toUpperCase();
        logger.info("^_^ the mac address is {}", macAddress);
        return macAddress;
    }

    public static void main(String[] args) throws UnknownHostException, SocketException {
        getLocalMacAddress();
    }
}