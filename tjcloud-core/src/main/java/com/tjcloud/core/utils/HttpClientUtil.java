package com.tjcloud.core.utils;

import com.alibaba.fastjson.JSON;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpClientUtil {
    private static final Logger logger = LoggerFactory.getLogger(HttpClientUtil.class);

    public static final int DEFAULT_TIMEOUT_SECONDS = 10000;

    public static void doGet(String uri, SortedMap<String, Object> parameters) {
        doGet(uri + "?" + join(parameters));
    }

    public static void doGet(String uri) {
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(10000).setConnectTimeout(10000).build();
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpRequest = new HttpGet(uri);
        httpRequest.setConfig(requestConfig);
        try {
            CloseableHttpResponse httpResponse = httpclient.execute((HttpUriRequest)httpRequest);
            try {
                logger.info(JSON.toJSONString(httpResponse.getStatusLine()));
                logger.info(JSON.toJSONString(httpResponse.getAllHeaders()));
                if (httpResponse.getStatusLine().getStatusCode() == 200) {
                    HttpEntity entity = httpResponse.getEntity();
                    if (entity != null) {
                        entity.getContent();
                        logger.info(EntityUtils.toString(entity));
                    }
                } else {
                    httpRequest.abort();
                }
            } finally {
                httpResponse.close();
            }
        } catch (ClientProtocolException e) {
            logger.error(e.getMessage(), (Throwable)e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public static String doPost(String uri, String json) {
        return doPost(uri, json, null);
    }

    public static String doPost(String uri, String json, String auth) {
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(10000).setConnectTimeout(10000).build();
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpRequest = new HttpPost(uri);
        httpRequest.setConfig(requestConfig);
        try {
            StringEntity se = new StringEntity(URLEncoder.encode(json, "UTF-8"));
            se.setContentEncoding((Header)new BasicHeader("Content-Type", "application/json"));
            if (StringUtils.isNotEmpty(auth))
                se.setContentEncoding((Header)new BasicHeader("Authorization", auth));
            httpRequest.setEntity((HttpEntity)se);
            CloseableHttpResponse httpResponse = httpclient.execute((HttpUriRequest)httpRequest);
            try {
                logger.info(JSON.toJSONString(httpResponse.getStatusLine()));
                logger.info(JSON.toJSONString(httpResponse.getAllHeaders()));
                if (httpResponse.getStatusLine().getStatusCode() == 200) {
                    HttpEntity entity = httpResponse.getEntity();
                    if (entity != null) {
                        logger.info(EntityUtils.toString(entity));
                        InputStream is = entity.getContent();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        int i = -1;
                        while ((i = is.read()) != -1)
                            baos.write(i);
                        String content = baos.toString("utf-8");
                        baos.close();
                        return content;
                    }
                } else {
                    httpRequest.abort();
                }
            } finally {
                httpResponse.close();
            }
        } catch (ClientProtocolException e) {
            logger.error(e.getMessage(), (Throwable)e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return "";
    }

    public static String join(SortedMap<String, Object> parameters) {
        StringBuffer buf = new StringBuffer();
        Set<Map.Entry<String, Object>> entrySet = parameters.entrySet();
        Iterator<Map.Entry<String, Object>> iterator = entrySet.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value != null && !"".equals(value))
                buf.append(key).append("=").append(value).append("&");
        }
        String result = buf.toString();
        return result.substring(0, result.length() - 1);
    }
}