package com.tjcloud.core.utils;

import com.alibaba.fastjson.JSON;
import java.io.IOException;
import java.util.Map;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonpRequest {
    private static final Logger logger = LoggerFactory.getLogger(JsonpRequest.class);

    public static String doGet(String url) {
        logger.debug("Jsonp post url :{}", url);
        try {
            Document result = Jsoup.connect(url).ignoreContentType(true).get();
            logger.debug("------ success ------");
            return result.text();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public static <T> T doGet(String url, Class<T> clazz) {
        String result = doGet(url);
        return StringUtils.isNotEmpty(result) ? (T)JSON.parseObject(result, clazz) : null;
    }

    public static String doPost(String url) {
        logger.debug("Jsonp post url :{}", url);
        try {
            Connection connection = Jsoup.connect(url).ignoreContentType(true);
            Document result = connection.post();
            logger.debug("------ success ------");
            return result.text();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public static <T> T doPost(String url, Class<T> clazz) {
        String result = doPost(url);
        return StringUtils.isNotEmpty(result) ? (T)JSON.parseObject(result, clazz) : null;
    }

    public static String doPost(String url, Map<String, String> data) {
        logger.debug("Jsonp post url :{}", url);
        try {
            Connection connection = Jsoup.connect(url).ignoreContentType(true);
            if (data != null)
                connection.data(data);
            Document result = connection.post();
            logger.debug("------ success ------");
            return result.text();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public static <T> T doPost(String url, Map<String, String> data, Class<T> clazz) {
        String result = doPost(url, data);
        return StringUtils.isNotEmpty(result) ? (T)JSON.parseObject(result, clazz) : null;
    }
}