package com.tjcloud.core.utils;

import com.tjcloud.core.exception.ValidationException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class ValidateUtils {
    public static void isTrue(boolean expression, int code) {
        isTrue(expression, code, "The validated expression is false");
    }

    public static void isTrue(boolean expression, int code, String message) {
        if (!expression)
            throw new ValidationException(Integer.valueOf(code), message);
    }

    public static void isTrue(boolean expression, int code, String message, Object... args) {
        if (!expression)
            throw new ValidationException(Integer.valueOf(code), message, args);
    }

    public static void isFalse(boolean expression, int code) {
        isTrue(expression, code, "The validated expression is false");
    }

    public static void isFalse(boolean expression, int code, String message) {
        if (expression == true)
            throw new ValidationException(Integer.valueOf(code), message);
    }

    public static void isFalse(boolean expression, int code, String message, Object... args) {
        if (expression == true)
            throw new ValidationException(Integer.valueOf(code), message, args);
    }

    public static void isNull(Object object, int code) {
        isNull(object, code, "The validated object is not null", new Object[0]);
    }

    public static void isNull(Object object, int code, String message, Object... args) {
        if (object != null)
            throw new ValidationException(Integer.valueOf(code), message, args);
    }

    public static void notNull(Object object, String message) {
        if (object == null)
            throw new IllegalArgumentException(message);
    }

    public static void notNull(Object object, int code) {
        notNull(object, code, "The validated object is null");
    }

    public static void notNull(Object object, int code, String message) {
        if (object == null)
            throw new ValidationException(Integer.valueOf(code), message);
    }

    public static void notNull(Object object, int code, String message, Object... args) {
        if (object == null)
            throw new ValidationException(Integer.valueOf(code), message, args);
    }

    @Deprecated
    public static void notNull(Object object) {
        notNull(object, 10100);
    }

    public static void notEmpty(Collection collection, int code) {
        notEmpty(collection, code, "The validated collection is empty");
    }

    public static void notEmpty(Collection collection, int code, String message) {
        if (collection == null || collection.size() == 0)
            throw new ValidationException(Integer.valueOf(code), message);
    }

    @Deprecated
    public static void notEmpty(Collection collection) {
        notEmpty(collection, 10101);
    }

    public static boolean notEmpty(Map map, int code) {
        return notEmpty(map, code, "The validated map is empty");
    }

    public static boolean notEmpty(Map map, int code, String message) {
        if (map == null || map.size() == 0)
            throw new ValidationException(Integer.valueOf(code), message);
        return true;
    }

    @Deprecated
    public static boolean notEmpty(Map map) {
        if (map == null || map.size() == 0)
            throw new IllegalArgumentException("The validated map is empty");
        return true;
    }

    public static void notEmpty(String string, int code) {
        notEmpty(string, code, "The validated string is empty");
    }

    public static void notEmpty(String string, int code, String message) {
        if (string == null || string.trim().length() == 0)
            throw new ValidationException(Integer.valueOf(code), message);
    }

    @Deprecated
    public static void notEmpty(String string) {
        notEmpty(string, 10101);
    }

    public static void noNullElements(Object[] array, int code) {
        noNullElements(array, code, "The validated array contains null element at index: ");
    }

    public static void noNullElements(Object[] array, int code, String message) {
        notNull(array, code);
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null)
                throw new ValidationException(Integer.valueOf(code), message + i);
        }
    }

    public static void isEmptyElements(Object[] array, int code) {
        notNull(array, code);
        boolean flag = true;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null)
                if (array[i] instanceof String) {
                    if (StringUtils.isNotEmpty((String)array[i])) {
                        flag = false;
                        break;
                    }
                } else {
                    flag = false;
                    break;
                }
        }
        if (flag)
            throw new ValidationException(Integer.valueOf(code));
    }

    public static void noNullElements(Collection collection, int code) {
        noNullElements(collection, code, "The validated collection contains null element at index: ");
    }

    public static void noNullElements(Collection collection, int code, String message) {
        notNull(collection, code);
        int i = 0;
        for (Iterator it = collection.iterator(); it.hasNext(); i++) {
            if (it.next() == null)
                throw new ValidationException(Integer.valueOf(code), message + i);
        }
    }

    public static void allElementsOfType(Collection collection, Class clazz, int code) {
        notNull(collection, code);
        notNull(clazz, code);
        int i = 0;
        for (Iterator it = collection.iterator(); it.hasNext(); i++) {
            if (!clazz.isInstance(it.next()))
                throw new ValidationException(Integer.valueOf(code), "The validated collection contains an element not of type " + clazz.getName() + " at index: " + i);
        }
    }

    public static void allElementsOfType(Collection collection, Class clazz, int code, String message) {
        notNull(collection, code);
        notNull(clazz, code);
        int i = 0;
        for (Iterator it = collection.iterator(); it.hasNext(); i++) {
            if (!clazz.isInstance(it.next()))
                throw new ValidationException(Integer.valueOf(code), message + "; " + clazz.getName() + " at index: " + i);
        }
    }

    public static void eq(String string, int len, int code) {
        int length = StringUtils.msNull(string).length();
        if (length != len)
            throw new ValidationException(Integer.valueOf(code), "The validated string length is " + length + ", expect length is " + length);
    }

    public static void min(String string, int min, int code) {
        int length = StringUtils.msNull(string).length();
        if (length < min)
            throw new ValidationException(Integer.valueOf(code), "The validated string length is " + length + ", min length is " + length);
    }

    public static void max(String string, int max, int code) {
        int length = StringUtils.msNull(string).length();
        if (length > max)
            throw new ValidationException(Integer.valueOf(code), "The validated string length is " + length + ", max length is " + length);
    }

    public static void min(int value, int min, int code) {
        if (value < min)
            throw new ValidationException(Integer.valueOf(code), "The validated value is " + value + ", min is " + min);
    }

    public static void max(int value, int max, int code) {
        if (value > max)
            throw new ValidationException(Integer.valueOf(code), "The validated value is " + value + ", max is " + max);
    }
}
