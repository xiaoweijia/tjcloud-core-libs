package com.tjcloud.core.utils;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ResourceUtils {
    private ResourceBundle resourceBundle;

    private ResourceUtils(String resource) {
        this.resourceBundle = ResourceBundle.getBundle(resource);
    }

    public static ResourceUtils getResource(String resource) {
        return new ResourceUtils(resource);
    }

    public String getValue(String key, Object[] args) {
        String temp = this.resourceBundle.getString(key);
        return MessageFormat.format(temp, args);
    }

    public Map<String, String> getMap() {
        Map<String, String> map = new HashMap<>();
        for (String key : this.resourceBundle.keySet())
            map.put(key, this.resourceBundle.getString(key));
        return map;
    }
}