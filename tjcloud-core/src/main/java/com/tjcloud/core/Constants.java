package com.tjcloud.core;

import java.nio.charset.Charset;

public class Constants {
  public static final int ONE_MINUTES_SECONDS = 60;

  public static final int TWO_MINUTES_SECONDS = 120;

  public static final int FIVE_MINUTES_SECONDS = 300;

  public static final int SIX_MINUTES_SECONDS = 360;

  public static final int TEN_MINUTES_SECONDS = 600;

  public static final int HALF_AN_HOUR_SECONDS = 1800;

  public static final int ONE_HOUR_SECONDS = 3600;

  public static final int TWO_HOUR_SECONDS = 7200;

  public static final int ONE_DAY_SECONDS = 86400;

  public static final int ONE_WEEK_SECONDS = 604800;

  public static final int ONE_MONTH_SECONDS = 2592000;

  public static final int ONE_YEAR_SECONDS = 31536000;

  public static final int ACCESS_TOKEN_EXPIRES_IN = 86400;

  public static final int REFRESH_TOKEN_EXPIRES_IN = 604800;

  public static final String YYYY_MM = "yyyy-MM";

  public static final String YYYY_MM_DD = "yyyy-MM-dd";

  public static final String YYYY_MM_DD_HH = "yyyy-MM-dd HH";

  public static final String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";

  public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

  public static final String YYYY = "yyyy";

  public static final String YYYYMM = "yyyyMM";

  public static final String YYYYMMDD = "yyyyMMdd";

  public static final String YYYYMMDDHH = "yyyyMMddHH";

  public static final String YYYYMMDDHHMM = "yyyyMMddHHmm";

  public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

  public static final String PV_COUNT = "pv";

  public static final String UV_COUNT = "uv";

  public static final String VV_COUNT = "vv";

  public static final String IP_COUNT = "ip";

  public static final String SIGN = "sign";

  public static final String MARK = "mark";

  public static final String RESPONSE_TEXT_OK = "ok";

  public static final String REFRESH_TOKEN_PREFIX = "retoken";

  public static final String SMS_CODE_PREFIX = "smscode";

  public static final String SMS_CODE_SEND = "sms_code";

  public static final String SMS_CODE_VALID = "sms_code_valid";

  public static final String SMS_CODE_LOGIN = "sms_code_login";

  public static final String SMS_RESET_PASSWORD = "sms_reset_pwd";

  public static final String SMS_SEND_MOBILE = "smsmo";

  public static final int DEFAULT_PCRIMG_CODE_WIDTH = 160;

  public static final int DEFAULT_PCRIMG_CODE_HEIGHT = 70;

  public static final String IMG_CODE_PREFIX = "imgcode";

  public static final String AUTH_CODE = "code";

  public static final String AUTH_MOBILE = "mobile";

  public static final String CHECK_MOBILE = "check_mobile";

  public static final String SMS_TYPE = "sms_type";

  public static final String RESPONSE_TYPE = "response_type";

  public static final String AUTH_ACCESS_TOKEN = "access_token";

  public static final String AUTH_CLIENT_ID = "client_id";

  public static final String AUTH_CLIENT_SECRET = "client_secret";

  public static final String AUTH_REDIRECT_URI = "redirect_uri";

  public static final String CAMEL_CREATED_DATE_START = "createdDateStart";

  public static final String CAMEL_CREATED_DATE_END = "createdDateEnd";

  public static final String CAMEL_CREATED_TIME_START = "createdTimeStart";

  public static final String CAMEL_CREATED_TIME_END = "createdTimeEnd";

  public static final String CAMEL_LAST_ACCESS = "lastAccess";

  public static final int ORDER_SEQUENCE_LENGTH = 16;

  public static final int TRADE_SEQUENCE_LENGTH = 18;

  public static final String ORDER_SEQUENCE_KEY = "order_sequence";

  public static final String TRADE_SEQUENCE_KEY = "trade_sequence";

  public static final String SYS_TOKEN_ID = "00000000";

  public static final String ANONYMOUS_USERA = "184c58cb65ee11e68fe10242ac11000a";

  public static final String ANONYMOUS_USERALIEN = "alien";

  public static final String RESOURCE_ACCESS_PREFIX = "resource_access";

  public static final String AUTH_ADMIN_ID = "c7297c6f271811e6a5140242ac110000";

  public static final String TENANT_ID = "tenantId";

  public static final String ATTR_ID = "id";

  public static final String AUTH_USER_ID = "userId";

  public static final String AUTH_USER_NAME = "username";

  public static final String AUTH_USER_NICK_NAME = "nickname";

  public static final String AUTH_USER_AVATAR = "avatar";

  public static final String CURRENT_TENANT_ID = "tenant_id";

  public static final String CURRENT_SESSION_UID = "uid";

  public static final String CURRENT_SESSION_UNAME = "uname";

  public static final String CURRENT_SESSION_NNAME = "nname";

  public static final String CURRENT_USER_NICK_NAME = "nick_name";

  public static final String CURRENT_USER_AVATAR = "auth_avatar";

  public static final String CURRENT_USER_ID = "user_id";

  public static final String CURRENT_USER_NAME = "user_name";

  public static final String CURRENT_AUTH_ID = "auth_id";

  public static final String CURRENT_AUTH_NAME = "auth_name";

  public static final String CURRENT_AUTH_TOKEN = "auth_token";

  public static final String CURRENT_AUTH_TOKEN_VALUE = "auth_token_value";

  public static final String CURRENT_REQUEST_METHOD = "request_method";

  public static final String CURRENT_REQUEST_URI = "request_uri";

  public static final String CURRENT_REQUEST_DOMAIN = "request_domain";

  public static final String PLATFORM_ADMIN_PREFIX = "platform_admin";

  public static final String TENANT_ADMIN_PREFIX = "tenant_admin";

  public static final String TENANT_ID_PREFIX = "tenant_id";

  public static final String STADIUM_IDS_PREFIX = "stadium_ids";

  public static final String STADIUM_ITEM_IDS_PREFIX = "stadium_item_ids";

  public static final String SOCKET_ONLINE_TENANT_ID = "socket_online_tenant_id";

  public static final String SOCKET_ONLINE_USER_COUNT = "socket_online_user_cont";

  public static final String ALIPAY_USER_ID = "alipay_user_id";

  public static final String ALIPAY_USER_AUTH_TOKEN = "alipay_user_auth_token";

  public static final String ALIPAY_USER_AUTH_REFRESH_TOKEN = "alipay_user_auth_retoken";

  public static final String ALIPAY_MERCH_AUTH_TOKEN = "alipay_merch_auth_token";

  public static final String ALIPAY_MERCH_AUTH_REFRESH_TOKEN = "alipay_merch_auth_retoken";

  public static final int DEFAULT_MOBILE_LENGTH = 11;

  public static final int PASSWORD_MIN_LENGTH = 6;

  public static final int RANDOM_CODE_LENGTH = 6;

  public static final String BODY_ATTR = "body";

  public static final String METHOD_ATTR = "method";

  public static final String RESPONSE_ATTR = "response";

  public static final String RESPONSE_SUCCESS_ATTR = "success";

  public static final String REQUEST_ATTR = "request";

  public static final String REQUEST_ID_ATTR = "request_id";

  public static final String REQUEST_START_ATTR = "request_start";

  public static final String REQUEST_AUTH_ID_ATTR = "authId";

  public static final String REQUEST_TOKEN_ID_ATTR = "tokenId";

  public static final String REQUEST_DAY_ZONE_ATTR = "dayzone";

  public static final String REQUEST_TIME_ZONE_ATTR = "timezone";

  public static final String REQUEST_OPTIONS_METHOD = "OPTIONS";

  public static final String REQUEST_ACCESS_TOKEN_ALIAS = "t";

  public static final String REQUEST_COOKIE_ACCESS_TOKEN_ALIAS = "cat";

  public static final String AVOID_DUPLICATE_SUBMIT_TICKET = "ticket";

  public static final Charset CHARACTER_SET = Charset.forName("utf-8");

  public static final class HttpMethod {
    public static final String POST = "POST";

    public static final String GET = "GET";

    public static final String DELETE = "DELETE";

    public static final String PUT = "PUT";
  }

  public static final class HeaderType {
    public static final String CONTENT_TYPE = "Content-Type";

    public static final String WWW_AUTHENTICATE = "WWW-Authenticate";

    public static final String AUTHORIZATION = "Authorization";

    public static final String X_TOKEN = "X-Token";
  }

  public static final class ContentType {
    public static final String URL_ENCODED = "application/x-www-form-urlencoded";

    public static final String JSON = "application/json";
  }

  public static final class TreeType {
    public static final String FOLDER = "folder";

    public static final String ITEM = "item";
  }

  public enum QUEUE {
    USER_LOGIN("用户登录", "user_login"),
    SMS_SENDING("短信发送", "sms_sending"),
    NOTICE_PUSH("消息提醒推送", "notice_push");

    private String code;

    private String text;

    QUEUE(String text, String code) {
      this.code = code;
      this.text = text;
    }

    public String getText() {
      return this.text;
    }

    public String toString() {
      return this.code;
    }
    }

  public enum LOGGING {
    MQ_LOGGING("消息日志", "mq_logging"),
    SMS_LOGGING("短信日志", "sms_logging"),
    PUSH_LOGGING("推送日志", "push_logging"),
    MAIL_LOGGING("邮件日志", "mail_logging"),
    OPS_LOGGING("操作日志", "ops_logging"),
    API_LOGGING("API调用日志", "api_logging");

    private String code;

    private String text;

    LOGGING(String text, String code) {
      this.code = code;
      this.text = text;
    }

    public String getText() {
      return this.text;
    }

    public String toString() {
      return this.code;
    }
    }
}