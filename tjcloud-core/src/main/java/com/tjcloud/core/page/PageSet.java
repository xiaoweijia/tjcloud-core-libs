package com.tjcloud.core.page;

import java.io.Serializable;
import java.util.List;

public class PageSet<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private int totalRows;

    private int pageSize;

    private int currPageNum;

    private int totalPages;

    private int startRow;

    private int endRow;

    private List<T> pageData;

    public PageSet(int totalRows, int currPageNum, int pageSize) {
        this.totalRows = totalRows;
        this.pageSize = pageSize;
        this.currPageNum = currPageNum;
        this.totalPages = totalRows / pageSize;
        if (totalRows % pageSize != 0)
            this.totalPages++;
        if (currPageNum <= 0)
            this.currPageNum = 1;
        if (this.totalPages < currPageNum)
            this.currPageNum = this.totalPages;
        this.startRow = (this.currPageNum - 1) * this.pageSize;
        this.endRow = this.startRow + this.pageSize;
        if (this.totalRows < this.endRow)
            this.endRow = this.totalRows;
    }

    public int getTotalRows() {
        return this.totalRows;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public int getCurrPageNum() {
        return this.currPageNum;
    }

    public int getTotalPages() {
        return this.totalPages;
    }

    public int getStartRow() {
        return this.startRow;
    }

    public int getEndRow() {
        return this.endRow;
    }

    public List<T> getPageData() {
        return this.pageData;
    }

    public void setPageData(List<T> pageData) {
        this.pageData = pageData;
    }
}