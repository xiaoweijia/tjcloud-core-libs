package com.tjcloud.core.message;

public abstract class MessageLocale {
  public static final int EMPTY_CODE = 0;

  public static final int INVALID_GRANT_TYPE = 10001;

  public static final int INVALID_AUTHORIZATION_CODE = 10002;

  public static final int INVALID_CLIENT_ID = 10003;

  public static final int INVALID_CLIENT_SECRET = 10004;

  public static final int INVALID_ACCESS_TOKEN = 10005;

  public static final int INVALID_REFRESH_TOKEN = 10006;

  public static final int INVALID_SMS_CODE = 10007;

  public static final int INVALID_OUT_USER_ID = 10008;

  public static final int INVALID_OUT_USER_TYPE = 10009;

  public static final int INVALID_USERNAE_OR_PASSWORD = 10010;

  public static final int INVALID_AUTH_USERNAME_NOT_EXISTS = 10011;

  public static final int INVALID_PARAMETER = 10012;

  public static final int INVALID_USERNAME_MOBILE = 10013;

  public static final int INVALID_ACCESS_TOKEN_ALLOW_TO_THE_URI = 10014;

  public static final int INVALID_USERINFO_PLEASE_LOGIN_AGAIN = 10015;

  public static final int INVALID_AUTH_ID_IS_EMTPTY = 10016;

  public static final int INVALID_USERNAME_MOBILE_IS_EMTPTY = 10017;

  public static final int INVALID_PASSWORD_IS_EMTPTY = 10018;

  public static final int NOT_NULL = 10100;

  public static final int NOT_EMPTY = 10101;

  public static final int LOCK_FAILED = 10102;

  public static final int UNLOCK_FAILED = 10103;

  public static final int BATCH_LOCK_FAILED = 10104;

  public static final int BATCH_UNLOCK_FAILED = 10105;

  public static final int QUERY_PARAMETERS_IS_EMPTY = 10106;

  public static final int SYSTEM_EXCEPTION = 10107;

  public static final int SUBMIT_TICKET_IS_EMPTY = 10108;

  public static final int REPEATED_SUBMIT_IS_NOT_ALLOWED = 10109;

  public static final int PAGE_NO_SIZE_NOT_FOUND = 10110;

  public static final int SYSTEM_IS_UPGRADING_PLEASE_TRY_AGAIN_LATER = 10111;

  public static final int YOU_MUST_UPGRADE_APP_VERSION = 10112;

  public static final int LDAP_LOGIN_FAILED_525 = 10200;

  public static final int LDAP_LOGIN_FAILED_52E = 10201;

  public static final int LDAP_LOGIN_FAILED_530 = 10202;

  public static final int LDAP_LOGIN_FAILED_532 = 10203;

  public static final int LDAP_LOGIN_FAILED_533 = 10204;

  public static final int LDAP_LOGIN_FAILED_701 = 10205;

  public static final int LDAP_LOGIN_FAILED_773 = 10206;
}