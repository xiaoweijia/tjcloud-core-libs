package com.tjcloud.core.exception;

public class ValidationException extends AbstractException {
    private static final long serialVersionUID = 3018621722418280712L;

    public ValidationException(Integer code) {
        super(code);
    }

    public ValidationException(Integer code, String message) {
        super(code, message);
    }

    public ValidationException(Integer code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public ValidationException(Integer code, String message, Object... args) {
        super(code, message, args);
    }

    public ValidationException(Integer code, String message, Throwable cause, Object... args) {
        super(code, message, cause, args);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}