package com.tjcloud.core.exception;

public class AuthException extends AbstractException {
    private static final long serialVersionUID = -3330376982837354681L;

    public AuthException(Integer code) {
        super(code);
    }

    public AuthException(Integer code, String message) {
        super(code, message);
    }

    public AuthException(Integer code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public AuthException(Integer code, String message, Object... args) {
        super(code, message, args);
    }

    public AuthException(Integer code, String message, Throwable cause, Object... args) {
        super(code, message, cause, args);
    }

    public AuthException(String message, Throwable cause) {
        super(message, cause);
    }
}