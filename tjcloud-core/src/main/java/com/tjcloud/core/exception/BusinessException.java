package com.tjcloud.core.exception;

public class BusinessException extends AbstractException {
    private static final long serialVersionUID = -3330376982837354681L;

    public BusinessException(Integer code) {
        super(code);
    }

    public BusinessException(Integer code, String message) {
        super(code, message);
    }

    public BusinessException(Integer code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public BusinessException(Integer code, String message, Object... args) {
        super(code, message, args);
    }

    public BusinessException(Integer code, String message, Throwable cause, Object... args) {
        super(code, message, cause, args);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
