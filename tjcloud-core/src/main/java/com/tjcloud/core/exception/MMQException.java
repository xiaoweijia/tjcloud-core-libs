package com.tjcloud.core.exception;

public class MMQException extends ServiceException {
    private static final long serialVersionUID = -2931774933427433664L;

    public MMQException(Integer code) {
        super(code);
    }

    public MMQException(Integer code, String message) {
        super(code, message);
    }

    public MMQException(Integer code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public MMQException(Integer code, String message, Object... args) {
        super(code, message, args);
    }

    public MMQException(Integer code, String message, Throwable cause, Object... args) {
        super(code, message, cause, args);
    }

    public MMQException(String message, Throwable cause) {
        super(message, cause);
    }
}